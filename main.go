// Package textile is helper for implements one-dimensional cellular automata.
//
// The textile package provides the feature to iteratively evaluate one-dimensional cellular automata.
package textile

import (
	"fmt"
)

// Stringify is used to indicates cell status when to string.
var Stringify = map[bool]string{
	true:  "1",
	false: "0",
}

type (
	// GetCell can decision how to get Cell on out of range Cells.
	// Should use this when get Cell from Cells.
	GetCell[Cell any] func(idx int, Cells []Cell) (int, Cell, bool)

	// Line indicates status for one-dimensional automata on current
	Line[Cell any, Option any] struct {
		Cells []Cell
		// GetCell decide how to get cell (on out of range Cells, especially).
		GetCell GetCell[Cell]
		Option  Option
	}

	// WeavingLine can be evaluated as a one-dimensional cellular automata.
	WeavingLine interface {
		// Evaluate must return next step, should not change on self.
		Evaluate() WeavingLine
		// String must return one-line text for indicates current self.
		String() string
	}
)

// Weave outputs text that indicated each step for one-dimensional cellular automata.
// And return latest step.
func Weave(n int, line WeavingLine) WeavingLine {
	for i := 0; i < n; i++ {
		fmt.Println(line)
		line = line.Evaluate()
	}

	return line
}

// GetNears return []Cell on "idx - near" to "idx + near".
// If "idx +- near" is out of range Cells, return is decided by GetCell.
func (l Line[Cell, Option]) GetNears(idx int, near int) ([]int, []Cell, []bool) {
	cells := make([]Cell, near*2+1)
	idxs := make([]int, near*2+1)
	exists := make([]bool, near*2+1)

	for i := 0; i < near*2+1; i++ {
		idx := idx - near + i
		idxs[i], cells[i], exists[i] = l.GetCell(idx, l.Cells)
	}

	return idxs, cells, exists
}

// GetCellCloseEdge is implementation of GetCell.
// If idx is out of range l, return empty value.
func GetCellCloseEdge[Cell any](idx int, l []Cell) (int, Cell, bool) {
	n := len(l)

	if idx < 0 || idx >= n {
		var empty Cell
		return 0, empty, false
	}

	return idx, l[idx], true
}

// GetCellCycleEdge is implementation of GetCell.
// If idx is out of range l, interpret the line as a ring and return a value.
func GetCellCycleEdge[Cell any](idx int, l []Cell) (int, Cell, bool) {
	n := len(l)

	f := func(i int, m []Cell) (int, Cell, bool) {
		return i, m[i], true
	}

	if idx < 0 {
		return f(n+idx, l)
	}

	if idx >= n {
		return f(idx-n, l)
	}

	return f(idx, l)
}
