// Implements diffusion simulator
package main

import (
	"math/rand"

	"gitlab.com/yoko-chance/textile"
)

func main() {
	n := 100
	cells := make([]Cell, n)
	for i := n*3/7; i < n*4/7; i++ {
		cells[i].amount = rand.Intn(100)
	}

	diffusion := Diffusion{Line{
		Cells: cells,
		GetCell: textile.GetCellCloseEdge[Cell],
		Option: Option{0.3, 0},
	}}

	textile.Weave(30, diffusion)
}


type (
	Option struct {
		// Diffusion coefficient
		Diffusion float64
		// Threshold for Cell that considered active.
		Threshold int
	}

	Cell struct {
		amount int
	}

	Line = textile.Line[Cell, Option]
	Diffusion struct {
		Line
	}
)

func (d Diffusion) Evaluate() textile.WeavingLine {
	n := len(d.Cells)
	next := make([]Cell, n)

	for i, cell := range d.Cells {
		if cell.amount <= 0 { continue }
		delta := int(float64(cell.amount) * d.Option.Diffusion)

		next[i].amount += cell.amount

		idxs, _, exists := d.GetNears(i, 1)
		for j := 0; j < len(idxs); j++ {
			if i == idxs[j] { continue }
			if !exists[j] { continue }

			next[i].amount -= delta
			next[idxs[j]].amount += delta
		}
	}

	return Diffusion{Line{
		Cells: next,
		GetCell: d.GetCell,
		Option: d.Option,
	}}
}

func (d Diffusion) String() string {
	n := len(d.Cells)
	s := ""

	for i := 0; i < n; i++ {
		s += textile.Stringify[d.Cells[i].amount > d.Option.Threshold]
	}

	return s
}
