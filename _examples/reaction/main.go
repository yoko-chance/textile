// Implements reaction simulator.
package main

import (
	"math/rand"

	"gitlab.com/yoko-chance/textile"
)

func main() {
	n := 100

	alpha := make([]Alpha, n)
	for i := 0; i < n; i++ {
		alpha[i].amount = rand.Intn(10)
	}

	beta := make([]Beta, n)
	for i := 0; i < n; i++ {
		beta[i].amount = rand.Intn(10)
	}

	gc := textile.GetCellCloseEdge[Matter]
	ma := Matters[Alpha]{AlphaLine{
		Cells: alpha,
		GetCell: gc,
		Option: MatterOption{},
	}}
	mb := Matters[Beta]{BetaLine{
		Cells: beta,
		GetCell: gc,
		Option: MatterOption{},
	}}

	v := make([]Virtual, n)
	reaction := Reaction{SetState(VirtualLine{
		Cells: v,
		GetCell: textile.GetCellCloseEdge[Virtual],
		Option: Option{ma, mb},
	})}

	textile.Weave(30, reaction)
}


type (
	MatterOption struct {}

	Matter struct {
		amount int
	}

	Alpha = Matter
	Beta  = Matter

	AlphaLine = textile.Line[Alpha, MatterOption]
	BetaLine = textile.Line[Beta, MatterOption]
	Matters[L Matter] struct {
		textile.Line[L, MatterOption]
	}
)

type (
	Option struct {
		alpha Matters[Alpha]
		beta  Matters[Beta]
	}

	Virtual struct {
		state bool
	}

	VirtualLine = textile.Line[Virtual, Option]
	Reaction struct {
		VirtualLine
	}
)

func (r Reaction) Evaluate() textile.WeavingLine {
	alpha := r.Option.alpha
	beta := r.Option.beta

	n := len(r.Cells)
	for i := 0; i < n; i++ {
		if alpha.Cells[i].amount > 0 && beta.Cells[i].amount > 0 {
			if alpha.Cells[i].amount >= beta.Cells[i].amount {
				alpha.Cells[i].amount -= beta.Cells[i].amount
			}
		}

		sum := 0
		for j := -4; j <= 4; j++ {
			if i+j >= 0 && i+j < n {
				sum += alpha.Cells[i+j].amount
			}
		}

		if sum < 30 {
			alpha.Cells[i].amount += sum / 4
		}
	}

	vl := SetState(VirtualLine{
		Cells: r.Cells,
		GetCell: r.GetCell,
		Option: Option{alpha, beta},
	})

	return Reaction{vl}
}

func SetState(l VirtualLine) VirtualLine {
	n := len(l.Cells)
	vl := VirtualLine{
		Cells: make([]Virtual, n),
		GetCell: l.GetCell,
		Option: Option{l.Option.alpha, l.Option.beta},
	}

	for i := 0; i < n; i++ {
		vl.Cells[i].state = (l.Option.alpha.Cells[i].amount >= 10)
	}

	return vl
}

func (r Reaction) String() string {
	n := len(r.Cells)
	s := ""

	for i := 0; i < n; i++ {
		s += textile.Stringify[r.Cells[i].state]
	}

	return s
}
