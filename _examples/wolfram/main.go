// Implements Wolfram cellular automata.
package main

import (
	"math/rand"

	"gitlab.com/yoko-chance/textile"
)

func main() {
	n := 100
	cells := make([]Cell, n)
	for i := 0; i < n; i++ {
		cells[i].state = rand.Intn(2)
	}

	rule := rand.Intn(256)
	println("Rule:" , rule)

	wolfram := &Wolfram{Line{
		Cells: cells,
		GetCell: textile.GetCellCycleEdge[Cell],
		Option: Option{rule},
	}}

	textile.Stringify = map[bool]string{
		true: "■",
		false: " ",
	}

	textile.Weave(30, wolfram)
}


type (
	Option struct {
		// Rule must falls in the range 0 to 255
		Rule int
	}

	Cell struct {
		// state must 0 or 1
		state int
	}

	Line = textile.Line[Cell, Option]
	Wolfram struct {
		Line
	}
)

func (w Wolfram) Evaluate() textile.WeavingLine {
	curr := w.Cells
	next := make([]Cell, len(curr))

	n := len(curr)
	r := w.Option.Rule

	// 自身と左右のcellの状態から次ステップの自身の状態を決定する
	for i := 0; i < n; i++ {
		_, near, _ := w.GetNears(i, 1)

		left := near[0].state
		center := near[1].state
		right := near[2].state

		next[i].state = (r >> (left*4+center*2+right)) & 1
	}

	return &Wolfram{Line{
		Cells: next,
		GetCell: w.GetCell,
		Option: w.Option,
	}}
}

func (w Wolfram) String() string {
	cells := w.Cells
	n := len(cells)
	s := ""

	for i := 0; i < n; i++ {
		s += textile.Stringify[cells[i].state == 1]
	}

	return s
}
