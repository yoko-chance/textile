package textile

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

type (
	Cell struct {
		state int
	}

	TLine = Line[Cell, any]
	Mock  struct {
		TLine
	}
)

func (m Mock) Evaluate() WeavingLine {
	next := make([]Cell, len(m.Cells))
	for i := 0; i < len(m.Cells); i++ {
		next[i].state = m.Cells[i].state + 1
	}

	return &Mock{TLine{
		Cells:   next,
		GetCell: m.GetCell,
		Option:  nil,
	}}
}

func (m Mock) String() string {
	str := ""
	for i := 0; i < len(m.Cells); i++ {
		str += Stringify[(m.Cells[i].state%2) == 0]
	}

	return str
}

func Test_GetCellCloseEdge(t *testing.T) {
	const (
		state_a = iota + 10
		state_b
		state_c
	)
	cells := make([]Cell, 2)
	cells[0].state = state_a
	cells[1].state = state_b

	type (
		in struct {
			idx   int
			cells []Cell
		}
		want struct {
			idx  int
			cell Cell
			ext  bool
		}
	)

	cases := []struct {
		title string
		in    in
		want  want
	}{
		{
			title: "a.1. 範囲内のindexを与えると返却される値はindex,cells[index],trueになる",
			in: in{
				idx:   0,
				cells: cells,
			},
			want: want{
				idx:  0,
				cell: cells[0],
				ext:  true,
			},
		},
		{
			title: "a.2. 範囲内のindexを与えると返却される値はindex,cells[index],trueになる",
			in: in{
				idx:   1,
				cells: cells,
			},
			want: want{
				idx:  1,
				cell: cells[1],
				ext:  true,
			},
		},
		{
			title: "b.1. 範囲外のindexを与えると返却される値は0,Cell{},falseになる",
			in: in{
				idx:   -1,
				cells: cells,
			},
			want: want{
				idx:  0,
				cell: Cell{},
				ext:  false,
			},
		},
		{
			title: "b.2. 範囲外のindexを与えると返却される値は0,Cell{},falseになる",
			in: in{
				idx:   2,
				cells: cells,
			},
			want: want{
				idx:  0,
				cell: Cell{},
				ext:  false,
			},
		},
	}

	for i := 0; i < len(cases); i++ {
		idx, cell, ext := GetCellCloseEdge(cases[i].in.idx, cases[i].in.cells)

		assert.Equal(t, cases[i].want.idx, idx, cases[i].title)
		assert.Equal(t, cases[i].want.cell, cell, cases[i].title)
		assert.Equal(t, cases[i].want.ext, ext, cases[i].title)
	}
}

func Test_GetCellCycleEdge(t *testing.T) {
	const (
		state_a = iota + 10
		state_b
	)
	cells := make([]Cell, 2)
	cells[0].state = state_a
	cells[1].state = state_b

	type (
		in struct {
			idx   int
			cells []Cell
		}
		want struct {
			idx  int
			cell Cell
			ext  bool
		}
	)

	cases := []struct {
		title string
		in    in
		want  want
	}{
		{
			title: "a.1. 範囲内のindexを与えると返却される値はindex,cells[index],trueになる",
			in: in{
				idx:   0,
				cells: cells,
			},
			want: want{
				idx:  0,
				cell: cells[0],
				ext:  true,
			},
		},
		{
			title: "a.2. 範囲内のindexを与えると返却される値はindex,cells[index],trueになる",
			in: in{
				idx:   1,
				cells: cells,
			},
			want: want{
				idx:  1,
				cell: cells[1],
				ext:  true,
			},
		},
		{
			title: "b.1. 下限未満のindexを与えると下限より下回った分だけ上限から数えたindexの値が返却される",
			in: in{
				idx:   -1,
				cells: cells,
			},
			want: want{
				idx:  1,
				cell: cells[1],
				ext:  true,
			},
		},
		{
			title: "b.1. 上限より大きいindexを与えると上限より上回った分だけ下限から数えたindexの値が返却される",
			in: in{
				idx:   2,
				cells: cells,
			},
			want: want{
				idx:  0,
				cell: cells[0],
				ext:  true,
			},
		},
	}

	for i := 0; i < len(cases); i++ {
		idx, cell, ext := GetCellCycleEdge(cases[i].in.idx, cases[i].in.cells)
		assert.Equal(t, cases[i].want.idx, idx, cases[i].title)
		assert.Equal(t, cases[i].want.cell, cell, cases[i].title)
		assert.Equal(t, cases[i].want.ext, ext, cases[i].title)
	}
}

func Test_GetNears(t *testing.T) {
	const (
		state_a = iota + 10
		state_b
		state_c
		state_d
		state_e
	)
	cells := make([]Cell, 5)
	cells[0].state = state_a
	cells[1].state = state_b
	cells[2].state = state_c
	cells[3].state = state_d
	cells[4].state = state_e

	type (
		in struct {
			idx     int
			near    int
			getcell GetCell[Cell]
		}
		want struct {
			idxs  []int
			cells []Cell
			exts  []bool
		}
	)

	cases := []struct {
		title string
		in    in
		want  want
	}{
		{
			title: "a.1. index-nearからindex+nearがすべて範囲内のとき、前述の範囲の値が返却される",
			in: in{
				idx:     2,
				near:    1,
				getcell: GetCellCloseEdge[Cell],
			},
			want: want{
				idxs:  []int{1, 2, 3},
				cells: []Cell{cells[1], cells[2], cells[3]},
				exts:  []bool{true, true, true},
			},
		},
		{
			title: "a.2. index-nearからindex+nearがすべて範囲内のとき、前述の範囲の値が返却される",
			in: in{
				idx:     3,
				near:    1,
				getcell: GetCellCloseEdge[Cell],
			},
			want: want{
				idxs:  []int{2, 3, 4},
				cells: []Cell{cells[2], cells[3], cells[4]},
				exts:  []bool{true, true, true},
			},
		},
		{
			title: "a.3. index-nearからindex+nearがすべて範囲内のとき、前述の範囲の値が返却される",
			in: in{
				idx:     2,
				near:    2,
				getcell: GetCellCloseEdge[Cell],
			},
			want: want{
				idxs:  []int{0, 1, 2, 3, 4},
				cells: []Cell{cells[0], cells[1], cells[2], cells[3], cells[4]},
				exts:  []bool{true, true, true, true, true},
			},
		},
		{
			title: "b.1. index-nearからindex+nearに範囲外の値が含まれるとき、その値に対応する返却値はGetCellに寄る",
			in: in{
				idx:     0,
				near:    1,
				getcell: GetCellCloseEdge[Cell],
			},
			want: want{
				idxs:  []int{0, 0, 1},
				cells: []Cell{Cell{}, cells[0], cells[1]},
				exts:  []bool{false, true, true},
			},
		},
		{
			title: "b.2. index-nearからindex+nearに範囲外の値が含まれるとき、その値に対応する返却値はGetCellに寄る",
			in: in{
				idx:     0,
				near:    1,
				getcell: GetCellCycleEdge[Cell],
			},
			want: want{
				idxs:  []int{4, 0, 1},
				cells: []Cell{cells[4], cells[0], cells[1]},
				exts:  []bool{true, true, true},
			},
		},
	}

	for i := 0; i < len(cases); i++ {
		line := TLine{cells, cases[i].in.getcell, nil}

		idxs, ncells, exts := line.GetNears(cases[i].in.idx, cases[i].in.near)

		assert.Equal(t, cases[i].want.idxs, idxs, cases[i].title)
		assert.Equal(t, cases[i].want.cells, ncells, cases[i].title)
		assert.Equal(t, cases[i].want.exts, exts, cases[i].title)
	}
}

func Test_Weave(t *testing.T) {
	cells := make([]Cell, 3)
	cells[1].state = 1

	m := Mock{TLine{cells, GetCellCloseEdge[Cell], nil}}

	type (
		in struct {
			step int
			line Mock
		}
		want struct {
			line WeavingLine
			out  string
		}
	)

	cases := []struct {
		title string
		in    in
		want  want
	}{
		{
			title: "ほげ",
			in: in{
				step: 3,
				line: m,
			},
			want: want{
				line: m.Evaluate().Evaluate().Evaluate(),
				out: m.String() + "\n" +
					m.Evaluate().String() + "\n" +
					m.Evaluate().Evaluate().String() + "\n",
			},
		},
	}

	for i := 0; i < len(cases); i++ {
		// 標準出力に出力される内容を取得する
		out := os.Stdout
		defer func() {
			os.Stdout = out
		}()
		r, w, _ := os.Pipe()
		os.Stdout = w

		next := Weave(cases[i].in.step, &m)

		w.Close()
		var buffer bytes.Buffer
		if _, err := buffer.ReadFrom(r); err != nil {
			t.Fatalf("fail read buf: %v", err)
		}
		s := buffer.String()

		assert.Equal(t, cases[i].want.out, s)
		assert.ObjectsAreEqualValues(cases[i].want.line, next)
	}
}
