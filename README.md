Textile
=======
Textileは1次元のセルオートマトンを作製するための枠組みを提供します

Usage
-----
```go
package main
import  "gitlab.com/yoko-chance/textile"

func main() {
	// セルの表示に使用する文字を設定できます
	textile.Stringify = map[bool]string{
		true: "W",
		false: "_",
	}

	// 初期状態を作製します
	sample := &Sample{Line{
		Cells: make([]Cell, 5),
		GetCell: textile.GetCellCycleEdge[Cell],
		Option: Option{},
	}}

	// 10ステップ分計算を行い結果を出力します
	textile.Weave(10, sample)
}

type (
	Option struct {
	}

	Cell struct {
		state bool
	}

	Line = textile.Line[Cell, Option]
	Sample struct {
		Line
	}
)

// 現在の状態をもとに次ステップの状態を作製します
func (s Sample) Evaluate() textile.WeavingLine {
	next := s.Cells
	for i := 0; i < len(s.Cells); i++ {
		next[i].state = !next[i].state
	}

	return &Sample{Line{
		Cells: next,
		GetCell: s.GetCell,
		Option: s.Option,
	}}
}

// 現在の状態を文字列として表現します
func (s Sample) String() string {
	str := ""
	for i := 0; i < len(s.Cells); i++ {
		str += textile.Stringify[s.Cells[i].state]
	}

	return str
}
```
